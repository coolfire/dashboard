var fs = require('fs');

var blessed = require('blessed')
  , contrib = require('blessed-contrib')
  , screen = blessed.screen()
  , grid = new contrib.grid({rows: 4, cols: 4, screen: screen})


const { spawnSync } = require( 'child_process' )

function fetch() {
  spawnSync( 'bundle', [ 'exec', 'ruby', 'fetch.rb' ] );

  lineData = JSON.parse(fs.readFileSync('main_loads.json', 'utf8'));
  line.setData(lineData);

  dockerLineData = JSON.parse(fs.readFileSync('docker_loads.json', 'utf8'));
  dockerLine.setData(dockerLineData);

  ircLineData = JSON.parse(fs.readFileSync('irc_loads.json', 'utf8'));
  ircLine.setData(ircLineData);

  vpnrates = JSON.parse(fs.readFileSync('vpn_bitrate.json', 'utf8'))[0];
  vpnsparkline.setData(['IN', 'OUT'], vpnrates)

  vpndonut.setData(JSON.parse(fs.readFileSync('vpn_bitrate_now.json', 'utf8')));

  wanrates = JSON.parse(fs.readFileSync('wan_bitrate.json', 'utf8'))[0];
  wansparkline.setData(['IN', 'OUT'], wanrates)

  wandonut.setData(JSON.parse(fs.readFileSync('wan_bitrate_now.json', 'utf8')));
}

var line = grid.set(0, 0, 2, 2, contrib.line,
  { style:
    {
      line: "yellow",
      text: "green",
      baseline: "black"
    },
    wholeNumbersOnly: false,
    xLabelPadding: 3,
    xPadding: 5,
    showLegend: true,
    label: 'Main server load'
  }
);

var dockerLine = grid.set(0, 2, 1, 2, contrib.line,
  { style:
    {
      line: "yellow",
      text: "green",
      baseline: "black"
    },
    wholeNumbersOnly: false,
    xLabelPadding: 3,
    xPadding: 5,
    showLegend: false,
    label: 'Docker swarm load'
  }
);

var ircLine = grid.set(1, 2, 1, 2, contrib.line,
  { style:
    {
      line: "yellow",
      text: "green",
      baseline: "black"
    },
    wholeNumbersOnly: false,
    xLabelPadding: 3,
    xPadding: 5,
    showLegend: false,
    label: 'IRC server load'
  }
);

var lineData       = null;
var dockerLineData = null;
var ircLineData    = null;

var vpnsparkline = grid.set(2, 0, 1, 1, contrib.sparkline,
  {
    label: 'VPN throughput',
    tags: true,
    style: { fg: 'green', titleFg: 'white' }
  }
);

var vpndonut = grid.set(2, 1, 1, 1, contrib.donut,
  {
    label: 'VPN utilization'
  }
);

var wansparkline = grid.set(3, 0, 1, 1, contrib.sparkline,
  {
    label: 'WAN throughput',
    tags: true,
    style: { fg: 'green', titleFg: 'white' }
  }
);

var wandonut = grid.set(3, 1, 1, 1, contrib.donut,
  {
    label: 'WAN utilization'
  }
);

fetch();

screen.key(['escape', 'q', 'C-c'], function(ch, key) {
  return process.exit(0);
});

screen.render()

setInterval(
  function() {
    fetch();
    screen.render();
  }
  , 60000
);

