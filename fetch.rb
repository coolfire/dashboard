#!/usr/bin/env ruby

require 'date'
require 'influxdb'

@influxdb = InfluxDB::Client.new 'librenms', host: 'influxdb.pve.insomnia247.nl'

def addload(servers)
  loads = []
  servers.each do |server|
    loads.push({
      'title': server[0],
      'x': [],
      'y': [],
      'style': { 'line': server[1] }
    })
  end
  loads
end

def addport(servers)
  ports = []
  servers.each do |server|
    ports.push([[],[]])
  end
  ports
end

def getloads(servers, loads)
  servers.each_with_index do |server, i|
    @influxdb.query "SELECT \"time\",\"5min\" FROM \"ucd_load\" WHERE \"hostname\" = '#{server[2]}' AND time > now() - 4h" do |_name, _tags, points|
      points.each do |point|
        dt    = DateTime.parse(point['time'])
        stamp = sprintf "%02d:%02d", dt.hour +  1, dt.minute
        lavg  = point['5min'] / 100.0
        loads[i][:x].push stamp
        loads[i][:y].push lavg
      end
    end
  end
  loads
end

def getports(servers, ports)
  servers.each_with_index do |server, i|
    @influxdb.query "SELECT \"time\",\"ifInOctets_rate\",\"ifOutOctets_rate\" FROM \"ports\" WHERE \"hostname\" = '#{server[2]}' AND \"ifName\" = '#{server[3]}' AND time > now() - 4h" do |_name, _tags, points|
      points.each do |point|
        dt      = DateTime.parse(point['time'])
        stamp   = sprintf "%02d:%02d", dt.hour +  1, dt.minute

	inrate  = sprintf "%.4f", (point['ifInOctets_rate'].to_f * 8 / 1024 / 1024)
        outrate = sprintf "%.4f", (point['ifOutOctets_rate'].to_f * 8 / 1024 / 1024)

        ports[i][0].push inrate
        ports[i][1].push outrate
      end
    end
  end
  ports
end

main_servers = [
  ['Lydia6', 'red',    'lydia6.insomnia247.nl'],
  ['Freya',  'yellow', 'freya.pve.insomnia247.nl'],
  ['Fay',    'green',  'fay.pve.insomnia247.nl']
]

main_loads = addload(main_servers)
main_loads = getloads(main_servers, main_loads)
File.write 'main_loads.json', main_loads.to_json

docker_servers = [
  ['n01', 'red',     'n01.swarm01.insomnia247.nl'],
  ['n02', 'yellow',  'n02.swarm01.insomnia247.nl'],
  ['n03', 'green',   'n03.swarm01.insomnia247.nl'],
  ['n04', 'blue',    'n04.swarm01.insomnia247.nl'],
  ['n05', 'magenta', 'n05.swarm01.insomnia247.nl'],
  ['n06', 'cyan',    'n06.swarm01.insomnia247.nl']
]

docker_loads = addload(docker_servers)
docker_loads = getloads(docker_servers, docker_loads)
File.write 'docker_loads.json', docker_loads.to_json

irc_servers = [
  ['n01', 'red',     'n01.leaf.irc.insomnia247.nl'],
  ['n02', 'yellow',  'n02.leaf.irc.insomnia247.nl']
]

irc_loads = addload(irc_servers)
irc_loads = getloads(irc_servers, irc_loads)
File.write 'irc_loads.json', irc_loads.to_json

vpn_ports = [
  ['VPN', '', '10.0.1.10', 'tun0']
]

vpn_bitrate = addport(vpn_ports)
vpn_bitrate = getports(vpn_ports, vpn_bitrate)
File.write 'vpn_bitrate.json', vpn_bitrate.to_json

File.write('vpn_bitrate_now.json', [
  {'percent': '%.4f' % ((vpn_bitrate[0][0].last).to_f / 100), label: 'IN', 'color': 'green'},
  {'percent': '%.4f' % ((vpn_bitrate[0][1].last).to_f / 100), label: 'OUT','color': 'cyan'}
].to_json)

wan_ports = [
  ['WAN', '', '10.0.0.1', 'enp4s0']
]

wan_bitrate = addport(wan_ports)
wan_bitrate = getports(wan_ports, wan_bitrate)
File.write 'wan_bitrate.json', wan_bitrate.to_json

File.write('wan_bitrate_now.json', [
  {'percent': '%.4f' % ((wan_bitrate[0][0].last).to_f / 100), label: 'IN', 'color': 'green'},
  {'percent': '%.4f' % ((wan_bitrate[0][1].last).to_f / 100), label: 'OUT','color': 'cyan'}
].to_json)

